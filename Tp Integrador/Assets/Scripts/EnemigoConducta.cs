using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoConducta : MonoBehaviour
{
    public float speed;
    public Rigidbody rbEnemigo;
    private bool chasing;
    public float distanceToChase = 10f, distanceToLose = 15f, distanceToStop;

    private Vector3 targetPoint, startPoint;

    public GameObject bullet;
    public Transform firePoint;

    public float fireRate;
    private float fireCount;

    public float keepChasingTime = 5f;
    private float chaseCounter;


    void Start()
    {
        startPoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        targetPoint = ControlJugador.instance.transform.position;
        targetPoint.y = transform.position.y;

        if(!chasing)
        {
            if(Vector3.Distance(transform.position, targetPoint) < distanceToChase)
            {
                chasing = true;

                fireCount = 1f;
            }
        }else
        {
            transform.LookAt(targetPoint);

            rbEnemigo.velocity = transform.forward * speed; 

            if(Vector3.Distance(transform.position, targetPoint) > distanceToLose)
            {
                chasing = false;


            }

            fireCount -= Time.deltaTime;
            {
                if(fireCount <= 0)
                {
                    fireCount = fireRate;
                    Instantiate(bullet, firePoint.position, firePoint.rotation);
                }
            }
        }
    }
}
